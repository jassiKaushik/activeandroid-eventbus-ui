package com.example.acer.activeandroid;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/**
 * Created by acer on 12-11-2016.
 */

public class ViewFragment extends Fragment {

    private ViewAdpater mAdapter;

    public  ViewFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rc);
        mAdapter = new ViewAdpater(new ArrayList<Books>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.getInstance().getEventBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().getEventBus().unregister(this);

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    // This method will be called when  posted
    public void onEventMainThread(ArrayList<Books> bookses) {
        mAdapter.addList(bookses);

    }


}
