package com.example.acer.activeandroid;

import android.content.ClipData;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acer on 12-11-2016.
 */

public class DaoController {

    private static DaoController _instance;
    private static String TAG = "DaoController";
    private static SQLiteDatabase database;

    public static DaoController getInstance() {
        if (_instance == null) {
            _instance = new DaoController();
            database = Application.getDatabase();
        }
        return _instance;
    }

    public void addSingleRow(Books book) {
        book.author = "abc";
        book.price = 123;
        book.title = "fgbjhnm";
        Log.d(TAG, "position of last record add" + book.save() + "");
    }

    public void addMultipleRow() {
        database.beginTransaction();
        try {
            for (int i = 0; i < 100; i++) {
                Books book = new Books();
                book.author = "abc" + i;
                book.price = 123 + i;
                book.title = "fgbjhnm" + i;
                Log.d(TAG, "position of last record add" + book.save() + "");
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();

        }

    }

    public List<Books> getALLRecords() {
        // return new Select().from(Books.class).orderBy("RANDOM()").execute();
        return new Select().from(Books.class).execute();

    }

    public void deleteSingleRow() {
        List<Books> books = getALLRecords();
        books.get(0).delete();
    }

    public void deleteMultipleRowOrQueringOnDeletion() {
       new Delete().from(Books.class).where("Price=?", 123).execute();
      // ;
    }

    public void updateSingleRow(int price){
        new Update(Books.class)
                .set("title = ?","jassi")
                .where("Price = ?", price)
                .execute();
    }


    public void updateMultipleRow(String name,int price){
        new Update(Books.class)
                .set("title = ?","jassi")
                .where("Price = ?", price)
                .execute();
    }
}
