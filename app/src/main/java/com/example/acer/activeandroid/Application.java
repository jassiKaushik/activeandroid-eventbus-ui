package com.example.acer.activeandroid;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.activeandroid.ActiveAndroid;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by acer on 11-11-2016.
 */

public class Application extends android.app.Application {

    private static SQLiteDatabase database;
    private static Application _instance;
    private static EventBus eventBus;


    public static Application getInstance() {
        if (_instance == null) {
            _instance=new Application();

        }
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        database = ActiveAndroid.getDatabase();
        eventBus = EventBus.getDefault();

    }

    public  static SQLiteDatabase getDatabase() {
        if (database == null) {
            database = ActiveAndroid.getDatabase();
        }
        return database;
    }

    public  EventBus getEventBus() {
        if (eventBus == null) {
            eventBus = EventBus.getDefault();
        }
        return eventBus;
    }

}
