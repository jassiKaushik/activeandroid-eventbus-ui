package com.example.acer.activeandroid;

/**
 * Created by acer on 11-11-2016.
 */

import android.os.Parcelable;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;


    // "name" has to finish with an "s", this is a mirror of the table named Books of your database
    @Table(name = "Books")
    public class Books extends Model {

        // This means that the table "Books" has a column named "Author"
        @Column(name = "Author")
        public String author;

        @Column(name = "Title")
        public String title;

        @Column(name = "Price")
        public int price;

        // public List<Page> pageList;

        public Books() {
            // You have to call super in each constructor to create the table.
            super();
        }

        public Books(String author, String title, int price) {
            super();
            this.author = author;
            this.title = title;
            this.price = price;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }




    }
