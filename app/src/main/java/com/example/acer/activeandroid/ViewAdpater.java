package com.example.acer.activeandroid;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acer on 12-11-2016.
 */

public class ViewAdpater extends RecyclerView.Adapter<ViewAdpater.MyViewHolder> {

    private List<Books> bookList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, bookPrice, author;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            author = (TextView) view.findViewById(R.id.author);
            bookPrice = (TextView) view.findViewById(R.id.bookPrice);
        }
    }


    public ViewAdpater(List<Books> bookList) {
        this.bookList = bookList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Books movie = bookList.get(position);
        holder.title.setText(movie.getTitle());
        holder.author.setText(movie.getAuthor());
        holder.bookPrice.setText(movie.getPrice() + "");
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public void addList(ArrayList<Books> bookses) {
        bookList.addAll(bookses);
        notifyDataSetChanged();
    }
}

