package com.example.acer.activeandroid;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by acer on 12-11-2016.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {


    public HomeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(getActivity());

    }

    public void initView(View view) {
        view.findViewById(R.id.insertSingle).setOnClickListener(this);
        view.findViewById(R.id.insertMultiple).setOnClickListener(this);
        view.findViewById(R.id.view).setOnClickListener(this);
        view.findViewById(R.id.deleteSingle).setOnClickListener(this);
        view.findViewById(R.id.deleteMultiple).setOnClickListener(this);
        view.findViewById(R.id.updateSingle).setOnClickListener(this);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        initView(view);
        return view;


    }

    @OnClick(R.id.insertSingle)
    private void insertSingleData() {
        Books books = new Books();
        DaoController.getInstance().addSingleRow(books);

    }


    @OnClick(R.id.insertMultiple)
    private void insertMultipleData() {
        DaoController.getInstance().addMultipleRow();
    }

    @OnClick(R.id.view)
    private List<Books> getRecords() {
        return DaoController.getInstance().getALLRecords();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.insertSingle:
                insertSingleData();
                break;
            case R.id.insertMultiple:
                insertMultipleData();
                break;
            case R.id.view:
                Fragment newFragment = new ViewFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction().replace(R.id.frameContainer, newFragment).addToBackStack(null);
                transaction.commit();

                Application.getInstance().getEventBus().postSticky(getRecords());
                break;
            case R.id.deleteSingle:
                DaoController.getInstance().deleteSingleRow();
                break;
            case R.id.deleteMultiple:
                DaoController.getInstance().deleteMultipleRowOrQueringOnDeletion();
                break;
            case  R.id.updateSingle:
                DaoController.getInstance().updateSingleRow(123);
                break;

        }
    }
}
